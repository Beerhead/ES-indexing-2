import argparse
from pathlib import Path
from elasticsearch import Elasticsearch
import warnings

warnings.filterwarnings("ignore")

cert = Path.cwd() / "certs/http_ca.crt"

parser = argparse.ArgumentParser(description='Simple CLI for searching in index, containing youtube videos sample data')
parser.add_argument("--elasticsearch_address", "-es_addr", help="Address of ES instance",
                    default='https://127.0.0.1:9200', required=False)
parser.add_argument("--certificate_path", "-cert_path", help="Path to auth certificate", default=cert, required=False)
parser.add_argument("-u", "--user", help="username to access ES", default='elastic', required=False)
parser.add_argument("-p", "--password", help="password to access ES", default="greatpass", required=False)
parser.add_argument("--use-or", help="use OR logic condition instead of AND", action="store_true")
parser.add_argument("--title-search", help="phrase for full text search in title", required=False)
parser.add_argument("--videoid", help="id of video", required=False)
parser.add_argument("--videokey", help="key theme of video", required=False)
parser.add_argument("--published-range",
                    help="range of pulication date, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty",
                    required=False)
parser.add_argument("--likes-range",
                    help="likes range of video, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty",
                    required=False)
parser.add_argument("--comments-range",
                    help="comments count of video, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty",
                    required=False)
parser.add_argument("--views-range",
                    help="views count of video, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty",
                    required=False)
parser.add_argument("--id-list", help="list of id of documents to return, provide integers, divided separated comma",
                    required=False)
parser.add_argument("--last-n-days", help="filter videos with publications for the last N days, provide N",
                    required=False)
parser.add_argument("--exact-phrase-match", help="phrase for exact search", required=False)
parser.add_argument("--distance-from-london",
                    help="pass distance in kilometres, to find videos published in specified range from London",
                    required=False)

args = parser.parse_args()
es = Elasticsearch(args.elasticsearch_address,
                   basic_auth=(args.user, args.password),
                   ca_certs=args.certificate_path
                   )


def main():
    logic = "should" if args.use_or else "must"

    def process_geo_distance_query(search_dict, query, col):
        search_dict["query"]['bool'][logic][query] = {"distance": f"{args.__dict__[arg]}km",
                                                      "Location": {
                                                          "lat": 50.51,
                                                          "lon": 0.13
                                                      }}

    def process_match_phrase_query(search_dict, query, col):
        search_dict["query"]['bool'][logic][query][col] = {"query": args.__dict__[arg]}

    def process_ids_query(search_dict, query, col):
        search_dict["query"]['bool'][logic][query] = {"values": args.__dict__[arg].split(",")}

    def process_match_query(search_dict, query, col):
        search_dict["query"]['bool'][logic][query][col] = {"query": args.__dict__[arg]}

    def process_term_query(search_dict, query, col):
        search_dict["query"]['bool'][logic][query][col] = {"value": args.__dict__[arg]}

    def process_range_query(search_dict, query, col):
        if arg == 'last_n_days':
            days = args.__dict__[arg]
            search_dict["query"]['bool'][logic][query][col] = {"gte": f"now-{days}d/d"}
        else:
            min, max = args.__dict__[arg].split(",")
            search_dict["query"]['bool'][logic][query][col] = {"gte": min if min else None, "lte": max if max else None}

    func_map = {
        "match": process_match_query,
        "term": process_term_query,
        "range": process_range_query,
        "ids": process_ids_query,
        "match_phrase": process_match_phrase_query,
        "geo_distance": process_geo_distance_query
    }

    queries = {"match": [{"col": "Title", "arg": "title_search"}],
               "term": [{"col": "Video ID", "arg": "videoid"}, {"col": "Keyword", "arg": "videokey"}],
               "range": [{"col": "Published At", "arg": "published_range"}, {"col": "Likes", "arg": "likes_range"},
                         {"col": "Comments", "arg": "comments_range"}, {"col": "Views", "arg": "views_range"},
                         {"col": "Published At", "arg": "last_n_days"}],
               "ids": [{"col": "query", "arg": "id_list"}],
               "match_phrase": [{"col": "Title", "arg": "exact_phrase_match"}],
               "geo_distance": [{"col": "Location", "arg": "distance_from_point"}]}

    search_dict = {"query": {"bool": {logic: {key: {} for key in queries.keys()}}}}
    for query, list_ in queries.items():
        for search_term in list_:
            arg = search_term["arg"]
            if args.__dict__.get(arg):
                col = search_term["col"]
                func_map[query](search_dict, query, col)

    search_dict['query']['bool'][logic] = [{k: v} for k, v in search_dict["query"]['bool'][logic].items() if v]
    return es.search(index="youtube_videos", body=search_dict).body


if __name__ == "__main__":
    resp = main()
    print(resp)
