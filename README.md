Main.py script parse csv file with youtube data and loads it into ES instance.
Youtube data was enriched with mocked geodata with faker, for geo query (find videos, provided within distance from London)



**usage: python youtube-cli.py**

-h, --help            show this help message and exit
  
  --elasticsearch_address ELASTICSEARCH_ADDRESS, -es_addr ELASTICSEARCH_ADDRESS
                        Address of ES instance

  --certificate_path CERTIFICATE_PATH, -cert_path CERTIFICATE_PATH
                        Path to auth certificate

  -u USER, --user USER  username to access ES

  -p PASSWORD, --password PASSWORD
                        password to access ES

  --use-or              use OR logic condition instead of AND

  --title-search TITLE_SEARCH
                        phrase for full text search in title

  --videoid VIDEOID     id of video

  --videokey VIDEOKEY   key theme of video

  --published-range PUBLISHED_RANGE
                        range of pulication date, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty

  --likes-range LIKES_RANGE
                        likes range of video, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty

  --comments-range COMMENTS_RANGE
                        comments count of video, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty

  --views-range VIEWS_RANGE
                        views count of video, provide 2 values splitted by comma. If limited only from one side leave unlimited side of range empty

  --id-list ID_LIST     list of id of documents to return, provide integers, divided separated comma

  --last-n-days LAST_N_DAYS
                        filter videos with publications for the last N days, provide N

  --exact-phrase-match EXACT_PHRASE_MATCH
                        phrase for exact search

  --distance-from-point DISTANCE_FROM_POINT
                        pass distance in kilometres, to find videos published in specified range from London
  
