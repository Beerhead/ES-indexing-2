import csv
from faker import Faker
from pathlib import Path
from elasticsearch import Elasticsearch
from itertools import chain

cert = Path.cwd() / "certs/http_ca.crt"
es = Elasticsearch('https://127.0.0.1:9200',
                   http_auth=("elastic", "greatpass"),
                   ca_certs=cert
                   )

faker = Faker()
def main():
    rows = list(csv.DictReader(open("data/videos-stats.csv")))
    for d in rows:
        loc = faker.location_on_land()
        d.update(Location={"lat": loc[0], "lon": loc[1]})
    mapping = {
        "properties":
            {
                "id:": {"type": "integer"},
                "Title": {"type": "text"},
                "Video ID": {"type": "keyword"},
                "Published At": {"type": "date"},
                "Keyword": {"type": "keyword"},
                "Likes": {"type": "integer"},
                "Comments": {"type": "text"},
                "Views": {"type": "integer"},
                "Location": {"type": "geo_point"}
            }
    }

    es.indices.create(index="youtube_videos", mappings=mapping)

    youtube_data = chain.from_iterable(
        ({"index": {"_index": "youtube_videos", "_id": str(ind)}}, d) for ind, d in enumerate(rows))
    es.bulk(body=youtube_data)


if __name__ == "__main__":
    main()
